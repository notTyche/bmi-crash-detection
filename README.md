# BMI Leisure | Crash Detection
This project is divided into 3 sub-projects: <br />
- [**PreProcessing**](./PreProcessing) - Pre-processing of the data <br />
- [**ModelTraining**](./ModelTraining) - Model training and evaluation <br />
- [**ModelExcution**](./ModelExecution) - Conversion of the model from Python to C# (and example of usage)  <br />
  

  Each sub-project has its own **config.ini** file that can be used to configure the project. The **config.ini** file is a simple text file that contains the configuration parameters in the following format:
  ```
    [Section]
    Parameter=Value
  ``` 

## Instructions
In order to create the model, you need to follow the following steps: <br />
1. Extract all zipped files from the logs folder into a two different folders: **training** and **test**. The **training** folder should contain all the logs that you want to use for training the model. The **test** folder should contain all the logs that you want to use for testing the model. <br />
2. Open the **config.ini** file in the **PreProcessing** project and set the **TRAIN_SET_INPUT_PATH** and **TEST_SET_INPUT_PATH** parameters to the paths of the folders that you created in the previous step. <br />
3. Execute the **PreProcessing** project with the desired thresholds to be used for the deterministic algorithm. The outputs of the project that you are interested in are the **RTLS-train.csv** and **RTLS-test.csv** files. These files contain the pre-processed data that will be used for training and testing the model. *(If you want to check the results on the shifted windows, they are diverted into several files, check code for more info)* <br />
4. Before training the model, you need to install the packages that are used in the **ModelTraining** project. You can do that by executing the following command:
```
pip install -r requirements.txt
```
5. In order to train the model, you need to execute one of the notebooks located in the **ModelTraining** project. The notebooks are named according to the architecture of the model that as to be trained *(the best one is the transformer model)*. Before executing the notebook, you need to set the **DATASET_PATH** parameter to the path of the folder that contains the **RTLS-train.csv** and **RTLS-test.csv** files. After executing the notebook, the model will be saved into a folder called **model** and located in the same folder of the dataset. <br />
6. Now, in order to use the model in C#, you need to install the following package by using pip:
```
pip install onnxruntime
```
7. After installing the package, you need to use it by executing the following command:
```
python -m tf2onnx.convert --saved-model <path to the model folder> --output <path to the output folder>/model.onnx 
```
8. Now you can use the model in C# by using the **ModelExecution** project. The project contains an example of how to use the model in C#. <br />


## Utilities
In the **PreProcessing** project, there are some utilities that can be used to generate logs from the windows of the dataset. This can be useful if you want to check the instances that are classified in a wrong way by the model. If you want to use these utilities, you need to follow the following steps: <br />
1. Open the **config.ini** file in the **PreProcessing** project and set the **WINDOWS_INPUT_PATH** parameter to the path of the dataset that contains the wrong classified windows. You can obtain this dataset from the output of the training of the model. <br />
2. Change the main program of the **PreProcessing** project by looking at the property on Visual Studio. You need to set the **SplitLogs** as the main program. <br />
3. Execute the **PreProcessing** project. The output of the project will be logs subdivided by each different transponder. <br />
4. Open the **config.ini** file in the **PreProcessing** project and set the **TRANSPONDERS_INPUT_PATH** parameter to the path of the logs that you have just generated. <br />
5. Change the main program of the **PreProcessing** project by looking at the property on Visual Studio. You need to set the **WindowsFromLog** as the main program. <br />
6. Execute the **PreProcessing** project. The output of the project will be the windows that you can use on our software to replay the instances that are classified in a wrong way by the model. <br />


## Credits
- Matteo Perfidio, University of Calabria | [LinkedIn](https://www.linkedin.com/in/matteo-perfidio/), [GitHub](https://github.com/notTyche), [Mail](mailto:perfidiomatteo7@gmail.com)

