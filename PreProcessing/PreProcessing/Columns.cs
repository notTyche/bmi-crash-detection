﻿
namespace PreProcessing {
    
    public class Columns
    {
        public static string[] GetHeatsColumns()
        {
            return new string[]
            {
                "TransponderId", "Timestamp", "Progress", "X", "Y", "RawPositionX", "RawPositionY", "IsMoving", "DistanceFromTheNearestPit", "Speed", "SignalExpired", "Movement", 

                "AccelerometerX", "AccelerometerY", "AccelerometerZ",

                "Crash", "FakeCrash", "CrashEvent", "IsFakeMoving", "Steering"
            };
        }

        public static string[] GetDatasetColumns()
        {
            return new string[]
            {
                "TransponderId", "Timestamp", "Progress", "X", "Y", "RawPositionX", "RawPositionY", "IsMoving", "DistanceFromTheNearestPit", "Speed", "SignalExpired", "Movement", 

                "AccelerometerX", "AccelerometerY", "AccelerometerZ",

                "IsFakeMoving", "Type", "Window", "Crash"
            };

        }

    }

}
