﻿namespace PreProcessing {

    public enum ModeType
    {
        ALL,
        HEATS,
        DATASET
    }
    
    public class Config
    {

        public Config()
        {

            LogsPath = "";
            TestSetPath = "";
            HeatsOutputPath = "";
            DatasetOutputPath = "";
            TranspondersOutputPath = "";
            TranspondersInputPath = "";
            WindowsInputPath = "";
            WindowsOutputPath = "";
            Mode = ModeType.ALL;
            Debug = false;
            SplitTrainingSet = false;

            var lines = File.ReadAllLines("config.ini");
            var context = false;

            foreach (string line in lines)
            {

                if (line.Equals("[PRE_PROCESSING]"))
                {
                    context = true;
                    continue;
                }

                if (context && line.StartsWith("["))
                {
                    break;
                }


                if (context) 
                {

                    string[] config = line.Split('=');

                    if (config[0] == "TRAIN_SET_INPUT_PATH")
                    {
                        LogsPath = config[1];
                    }
                    if (config[0] == "TEST_SET_INPUT_PATH")
                    {
                        TestSetPath = config[1];
                    }
                    else if (config[0] == "HEATS_OUTPUT_PATH")
                    {
                        HeatsOutputPath = config[1];
                    }
                    else if (config[0] == "DATASET_OUTPUT_PATH")
                    {
                        DatasetOutputPath = config[1];
                    }
                    else if (config[0] == "MODE")
                    {
                        Mode = (ModeType) Enum.Parse(typeof(ModeType), config[1]);
                    }
                    else if (config[0] == "DEBUG")
                    {
                        Debug = config[1] == "1";
                    }
                    else if (config[0] == "SPLIT_TRAINING_SET")
                    {
                        SplitTrainingSet = config[1] == "1";
                    }
                    else if (config[0] == "TRANSPONDERS_OUTPUT_PATH")
                    {
                        TranspondersOutputPath = config[1];
                    }
                    else if (config[0] == "TRANSPONDERS_INPUT_PATH")
                    {
                        TranspondersInputPath = config[1];
                    }
                    else if (config[0] == "WINDOWS_INPUT_PATH")
                    {
                        WindowsInputPath = config[1];
                    }
                    else if (config[0] == "WINDOWS_OUTPUT_PATH")
                    {
                        WindowsOutputPath = config[1];
                    }


                }
            }
           
        }

        public string LogsPath { get; set; }
        public string TestSetPath { get; set; }
        public string HeatsOutputPath { get; set; }
        public string DatasetOutputPath { get; set; }
        public string TranspondersOutputPath { get; set; }
        public string TranspondersInputPath { get; set; }
        public string WindowsInputPath { get; set; }
        public string WindowsOutputPath { get; set; }
        public ModeType Mode { get; set; }
        public bool Debug { get; set;}
        public bool SplitTrainingSet { get; set; }

    }

}
