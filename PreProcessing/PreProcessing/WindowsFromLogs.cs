﻿using System.Globalization;

namespace PreProcessing {

    class WindowsFromLogs {

        class Pair<T, U> {

            public Pair(T first, U second) {
                First = first;
                Second = second;
            }

            public T First { get; set; }
            public U Second { get; set; }

        }

        class Tuple<T, U, V, Z> {

            public Tuple(T first, U second, V third, Z fourth) { 
                First = first;
                Second = second;
                Third = third;
                Fourth = fourth;
            }

            public T First { get; set; }
            public U Second { get; set; }
            public V Third { get; set; }
            public Z Fourth { get; set; }

        }

        class WrongRow {

            public WrongRow(string aType, string aTransponderId, string aWindow, string aStartDate, string aEndDate) {
                Type = aType;
                TransponderId = aTransponderId;
                Window = aWindow;
                StartDate = DateTime.ParseExact(aStartDate, "yyyy/MM/dd HH:mm:ss:fff", CultureInfo.InvariantCulture);
                EndDate = DateTime.ParseExact(aEndDate, "yyyy/MM/dd HH:mm:ss:fff", CultureInfo.InvariantCulture);
            }

            public string Type { get; set; }
            public string TransponderId { get; set; }
            public string Window { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }

        }

        static void Main(string[] args)
        {

            var config = new Config();
            var files = Directory.GetFiles(config.TranspondersInputPath);

            var wrongDataset = new List<WrongRow>();

            using (StreamReader sr = File.OpenText(config.WindowsInputPath))
            {

                System.Console.WriteLine("[INFO] Reading wrong dataset: " + config.WindowsInputPath);

                var s = "";
                var count = 0;

                while ((s = sr.ReadLine()) != null)
                {
                    if (count == 0)
                    {
                        count++;
                        continue;
                    }

                    var line = s.Split(',');

                    if (line.Length >= 5)
                    {
                        wrongDataset.Add(new WrongRow(line[0], line[1], line[2], line[3], line[4]));
                    }

                }

                System.Console.WriteLine("[INFO] Wrong dataset read: " + config.WindowsInputPath);
            }
            

            foreach (string file in files)
            {
                
                var transponderId = file.Split('\\').Last().Split('.')[0].Replace("-", ":");
                var logs = new List<string>();

                System.Console.WriteLine("[INFO] Reading log: " + file);

                using (StreamReader sr = File.OpenText(file))
                {
                    var s = "";

                    while ((s = sr.ReadLine()) != null)
                    {
                        logs.Add(s);   
                    }
                }

                System.Console.WriteLine("[INFO] Log read: " + file);

                var windows = new List<Pair<Tuple<string, string, string, DateTime>, List<string>>>();

                System.Console.WriteLine("[INFO] Creating windows for transponder: " + transponderId);

                foreach (var row in wrongDataset)
                {
                        
                    if (row.TransponderId == transponderId)
                    {

                        var type = row.Type;
                        var startDate = row.StartDate;
                        var endDate = row.EndDate;
                        var window = row.Window;

                        var logsInWindow = new List<string>();

                        foreach (var log in logs)
                        {

                            DateTime logDate = DateTime.MinValue;

                            if (log.StartsWith("TP"))
                            {
                                var line = log.Split(',');

                                line[7] = line[7].Replace('T', ' ');
                                line[7] = line[7].Replace('-', '/');
                                line[7] = line[7].Replace('.', ':');

                                if (line.Length >= 11)
                                {
                                    logDate = DateTime.ParseExact(line[7], "yyyy/MM/dd HH:mm:ss:fff", CultureInfo.InvariantCulture);
                                }

                            }
                            else if (log.StartsWith("|"))
                            {

                                var partitions = log.Split('\t');
                                partitions[0] = partitions[0].Replace("|", "");

                                var line = partitions[0].Split(',');

                                if (line.Length == 13)
                                {
                                    logDate = DateTime.ParseExact(line[1].Substring(0, line[1].Length - 1), "yyyy/MM/dd HH:mm:ss:fff", CultureInfo.InvariantCulture);
                                }
                            }
                            
                            if (logDate >= startDate - TimeSpan.FromSeconds(30) && logDate <= endDate + TimeSpan.FromSeconds(30))
                            {
                                logsInWindow.Add(log);
                            }

                        }

                        windows.Add(new Pair<Tuple<string, string, string, DateTime>, List<string>>(new Tuple<string, string, string, DateTime>(type, transponderId, window, endDate), logsInWindow));

                    }
    
                }

                System.Console.WriteLine("[INFO] Windows created for transponder: " + transponderId);

                System.Console.WriteLine("[INFO] Writing windows for transponder: " + transponderId);

                foreach (var window in windows)
                {

                    var type = window.First.First;
                    var transponderId2 = window.First.Second;
                    var window2 = window.First.Third;
                    var date = window.First.Fourth.ToString("HH-mm-ss-fff");
                    var logsInWindow = window.Second;

                    var path = config.WindowsOutputPath + "\\" + type + "\\" + transponderId2.Replace(":", "-") + "-" + window2 + "-" + date + ".txt";

                    if (!Directory.Exists(config.WindowsOutputPath + "\\" + type))
                    {
                        Directory.CreateDirectory(config.WindowsOutputPath + "\\" + type);
                    }

                    using (StreamWriter sw = File.AppendText(path))
                    {

                        foreach (var log in logsInWindow)
                        {
                            sw.WriteLine(log);
                        }

                    }

                }

                System.Console.WriteLine("[INFO] Windows written for transponder: " + transponderId);

            }

            System.Console.WriteLine("[INFO] Windows created for all transponders");

        }




    }
}
