﻿using System;
using System.Globalization;

namespace PreProcessing {
    
    class SplitLogs {

        class Pair<T, U> {

            public Pair(T first, U second) {
                First = first;
                Second = second;
            }

            public T First { get; set; }
            public U Second { get; set; }
        }
        
        static void Main(string[] args)
        {

            var config = new Config();
            var files = Directory.GetFiles(config.TestSetPath);

            var rows = new List<Pair<string, List<string>>>();
            var transponderIds = new List<string>();

            if (!Directory.Exists(config.TranspondersOutputPath))
            {
                Directory.CreateDirectory(config.TranspondersOutputPath);
                System.Console.WriteLine("[INFO] Created directory: " + config.TranspondersOutputPath);
            }

            foreach (string file in files)
            {

                if (file.EndsWith(".txt"))
                {

                    System.Console.WriteLine("[INFO] Splitting log: " + file);

                    using (StreamReader sr = File.OpenText(file))
                    {
                        var s = "";

                        while ((s = sr.ReadLine()) != null)
                        {

                            var transponderId = "";

                            if (s.StartsWith("TP"))
                            {
                                var line = s.Split(',');

                                if (line.Length >= 11)
                                {
                                    transponderId = line[1].Replace(":", "-");    
                                }

                            }
                            else if (s.StartsWith("|"))
                            {
                                var partitions = s.Split('\t');
                                partitions[0] = partitions[0].Replace("|", "");

                                var line = partitions[0].Split(',');

                                if (line.Length == 13)
                                {
                                    transponderId = line[0].Replace(":", "-");
                                }
                            }

                            if (transponderId != "")
                            {
                                
                                if (!transponderIds.Contains(transponderId))
                                {
                                    transponderIds.Add(transponderId);
                                    rows.Add(new Pair<string, List<string>>(transponderId, new List<string>()));
                                }
                                else
                                {
                                    var index = transponderIds.IndexOf(transponderId);
                                    rows[index].Second.Add(s);
                                }

                            }

                        }

                    }

                    System.Console.WriteLine("[INFO] Finished splitting log: " + file);

                }

            }

            System.Console.WriteLine("[INFO] Creating files...");

            foreach (var row in rows)
            {

                var path = config.TranspondersOutputPath + "//" + row.First.Replace(":", "-") + ".txt";

                using (StreamWriter sw = File.CreateText(path))
                {
                    foreach (var line in row.Second)
                    {
                        sw.WriteLine(line);
                    }
                }

                System.Console.WriteLine("[INFO] Created file: " + path);

            }

            System.Console.WriteLine("[INFO] Finished splitting logs.");

        }

    }

}
