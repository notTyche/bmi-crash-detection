﻿using PreProcessing;
using System.Globalization;

namespace PreProcessing {
    
    class Program {

        const int WINDOW_SIZE_OUTPUT = 50;
        const int WINDOW_SIZE_CRASH = 10;
        const int WINDOW_SIZE_UNCRASH = 30;
        const int WINDOW_SIZE_CHECK_CRASH = 50;
        const double THRESHOLD_MOVEMENT_CRASH = 800;
        const double THRESHOLD_MOVEMENT_FAKE_CRASH = 6000;
        const double THRESHOLD_MOVEMENT_RANDOM_SAMPLE = 12000;
        const double THRESHOLD_NEAREST_PIT = 10;
        const int EVENT_SHIFT = 5;
        const int SPLIT_FACTOR = 10;

        static int _windowCount = 0;

        enum CrashType
        {
            REAL,
            FAKE, 
            RANDOM
        }

        struct Point3D 
        {
            public Point3D(double aX, double aY, double aZ)
            {
                X = aX;
                Y = aY;
                Z = aZ;
            }

            public double X { get; }
            public double Y { get; }
            public double Z { get; }

        }

        class Pair<T, U> {

            public Pair(T first, U second) {
                First = first;
                Second = second;
            }

            public T First { get; set; }
            public U Second { get; set; }
        }

        class RawData
        {

            public RawData(string aTransponderId, string aRawPositionX, string aRawPositionY, string aTimestamp, Point3D aAccelerometer)
            {
                TransponderId = aTransponderId;
                RawPositionX = double.Parse(aRawPositionX, CultureInfo.InvariantCulture);
                RawPositionY = double.Parse(aRawPositionY, CultureInfo.InvariantCulture);
                Timestamp = DateTime.ParseExact(aTimestamp, "yyyy/MM/dd HH:mm:ss:fff", CultureInfo.InvariantCulture);
                Accelerometer = aAccelerometer;
            }

            public string TransponderId { get; set; }
            public double RawPositionX { get; set; }
            public double RawPositionY { get; set; }
            public DateTime Timestamp { get; set; }
            public Point3D Accelerometer { get; set; }

        }

        class ProcessedData
        {

            public ProcessedData(string aTransponderId, string aProgress, string aX, string aY, string aDistanceFromTheNearestPit, string aSpeed, string aSignalExpired, string aMovement, string aIsCrashEvent)
            {
                TransponderId = aTransponderId;
                Progress = double.Parse(aProgress, CultureInfo.InvariantCulture);
                X = double.Parse(aX, CultureInfo.InvariantCulture);
                Y = double.Parse(aY, CultureInfo.InvariantCulture);
                IsMoving = double.Parse(aMovement, CultureInfo.InvariantCulture) > THRESHOLD_MOVEMENT_CRASH ? true : false;
                IsFakeMoving = double.Parse(aMovement, CultureInfo.InvariantCulture) > THRESHOLD_MOVEMENT_FAKE_CRASH ||
                            double.Parse(aMovement, CultureInfo.InvariantCulture) <= THRESHOLD_MOVEMENT_CRASH ? true : false;
                IsRandomMoving = double.Parse(aMovement, CultureInfo.InvariantCulture) > THRESHOLD_MOVEMENT_RANDOM_SAMPLE ||
                            double.Parse(aMovement, CultureInfo.InvariantCulture) <= THRESHOLD_MOVEMENT_FAKE_CRASH ? true : false;
                DistanceFromTheNearestPit = double.Parse(aDistanceFromTheNearestPit, CultureInfo.InvariantCulture);
                Speed = double.Parse(aSpeed, CultureInfo.InvariantCulture);
                SignalExpired = aSignalExpired == "1" ? true : false;
                Movement = double.Parse(aMovement, CultureInfo.InvariantCulture);
                IsCrashEvent = aIsCrashEvent == "1" ? true : false;
            }

            public string TransponderId { get; set; }
            public double Progress { get; set; }
            public double X { get; set; }
            public double Y { get; set; }
            public bool IsMoving { get; set; }
            public bool IsFakeMoving { get; set; }
            public bool IsRandomMoving { get; set; }
            public double DistanceFromTheNearestPit { get; set; }
            public double Speed { get; set; }
            public bool SignalExpired { get; set; }
            public double Movement { get; set; }
            public bool IsCrashEvent { get; set; }

        }

        class Row : ICloneable
        {

            public Row(string aTransponderId, double aRawPositionX, double aRawPositionY, DateTime aTimestamp, Point3D aAccelerometer, double aX, double aY, bool aIsMoving, double aProgress, double aDistanceFromTheNearestPit, double aSpeed, bool aSignalExpired, double aMovement, bool aIsCrashEvent, bool aIsFakeMoving, bool aIsRandomMoving)
            {
                TransponderId = aTransponderId;
                RawPositionX = aRawPositionX;
                RawPositionY = aRawPositionY;
                Timestamp = aTimestamp;
                Accelerometer = aAccelerometer;
                X = aX;
                Y = aY;
                IsMoving = aIsMoving;
                Progress = aProgress;
                DistanceFromTheNearestPit = aDistanceFromTheNearestPit;
                Speed = aSpeed;
                SignalExpired = aSignalExpired;
                Movement = aMovement;
                IsCrash = false;
                IsCrashEvent = aIsCrashEvent;
                IsFakeCrash = false;
                IsFakeMoving = aIsFakeMoving;
                IsRandomCrash = false;
                IsRandomMoving = aIsRandomMoving;
                IsSlowDown = false;
                Steering = false;
                Window = 0;
                WindowLabel = false;
                Type = "";
            }

            public string TransponderId { get; set; }
            public double RawPositionX { get; set; }
            public double RawPositionY { get; set; }
            public DateTime Timestamp { get; set; }
            public Point3D Accelerometer { get; set; }
            public double X { get; set; }
            public double Y { get; set; }
            public bool IsMoving { get; set; }
            public bool IsFakeMoving { get; set; }
            public bool IsRandomMoving { get; set; }
            public double Progress { get; set; }
            public double DistanceFromTheNearestPit { get; set; }
            public double Speed { get; set; }
            public bool SignalExpired { get; set; }
            public double Movement { get; set; }
            public bool IsCrash { get; set; }
            public bool IsFakeCrash { get; set; }
            public bool IsSlowDown { get; set; }
            public bool IsRandomCrash { get; set; }
            public bool IsCrashEvent { get; set; }
            public bool Steering { get; set; }
            public int Window { get; set; }
            public bool WindowLabel { get; set; }
            public string Type { get; set; }

            public object Clone()
            {
                var clone = new Row(TransponderId, RawPositionX, RawPositionY, Timestamp, Accelerometer, X, Y, IsMoving, Progress, DistanceFromTheNearestPit, Speed, SignalExpired, Movement, IsCrashEvent, IsFakeMoving, IsRandomMoving);
                clone.IsCrash = IsCrash;
                clone.IsFakeCrash = IsFakeCrash;
                clone.IsRandomCrash = IsRandomCrash;
                clone.Window = Window;
                clone.WindowLabel = WindowLabel;
                clone.Steering = Steering;
                clone.IsSlowDown = IsSlowDown;
                return clone;
            }

            public string[] GetHeatsValues()
            {
                return new string[] 
                { 
                    TransponderId, 
                    Timestamp.ToString("yyyy/MM/dd HH:mm:ss:fff"), 
                    Progress.ToString().Replace(",", "."),
                    X.ToString().Replace(",", "."), 
                    Y.ToString().Replace(",", "."),
                    RawPositionX.ToString().Replace(",", "."), 
                    RawPositionY.ToString().Replace(",", "."), 
                    IsMoving ? "1" : "0", 
                    DistanceFromTheNearestPit.ToString().Replace(",", "."), 
                    Speed.ToString().Replace(",", "."), 
                    SignalExpired ? "1" : "0",
                    Movement.ToString().Replace(",", "."),
                    Accelerometer.X.ToString().Replace(",", "."), 
                    Accelerometer.Y.ToString().Replace(",", "."), 
                    Accelerometer.Z.ToString().Replace(",", "."),
                    IsCrash ? "1" : "0",
                    IsFakeCrash ? "1" : "0",
                    IsCrashEvent ? "1" : "0",
                    IsFakeMoving ? "1" : "0",
                    Steering ? "1" : "0"
                };
            }

            public string[] GetDatasetValues()
            {
                return new string[] 
                { 
                    TransponderId, 
                    Timestamp.ToString("yyyy/MM/dd HH:mm:ss:fff"), 
                    Progress.ToString().Replace(",", "."),
                    X.ToString().Replace(",", "."), 
                    Y.ToString().Replace(",", "."),
                    RawPositionX.ToString().Replace(",", "."), 
                    RawPositionY.ToString().Replace(",", "."), 
                    IsMoving ? "1" : "0", 
                    DistanceFromTheNearestPit.ToString().Replace(",", "."), 
                    Speed.ToString().Replace(",", "."), 
                    SignalExpired ? "1" : "0",
                    Movement.ToString().Replace(",", "."),
                    Accelerometer.X.ToString().Replace(",", "."), 
                    Accelerometer.Y.ToString().Replace(",", "."), 
                    Accelerometer.Z.ToString().Replace(",", "."),
                    IsFakeMoving ? "1" : "0",
                    Type,
                    Window.ToString(),
                    WindowLabel ? "1" : "0"
                };
            }
            
        }

        static Point3D ParsePoint(string payload)
        {

            var x = 0.0d;
            var y = 0.0d;
            var z = 0.0d;

            if (!string.Equals(payload, "null", StringComparison.OrdinalIgnoreCase))
            {

                int? accAxisValueLength = null;

                var aX = "";
                var aY = "";
                var aZ = "";


                if (payload.Length == 14)
                {
                    accAxisValueLength = 4;
                }
                else if (payload.Length == 26)
                {
                    accAxisValueLength = 8;
                }

                if (accAxisValueLength.HasValue)
                {
                    aX = payload.Substring(2, accAxisValueLength.Value);
                    aY = payload.Substring(2 + accAxisValueLength.Value, accAxisValueLength.Value);
                    aZ = payload.Substring(2 + accAxisValueLength.Value * 2, accAxisValueLength.Value);

                    x = int.Parse(aX, NumberStyles.HexNumber);
                    y = int.Parse(aY, NumberStyles.HexNumber);
                    z = int.Parse(aZ, NumberStyles.HexNumber);

                }

                if (x > 60000)
                {
                    x -= 65536;
                }

                if (y > 60000)
                {
                    y -= 65536;
                }

                if (z > 60000)
                {
                    z -= 65536;
                }

            }

            return new Point3D(x, y, z);
        }

        static List<string> GetAllTransponder(List<Row> rows)
        {

            var transponders = new List<string>();

            for (int i = 0; i < rows.Count; i++)
            {
                if (!transponders.Contains(rows[i].TransponderId))
                {
                    transponders.Add(rows[i].TransponderId);
                }
            }

            return transponders;

        }

        static bool IsCrash(List<Row> rows, CrashType type, int index, int isMovingWindow)
        {

            if (index < 0 || index >= rows.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            var start = index;
            var end = index + isMovingWindow - 1;

            if (end >= rows.Count || start >= rows.Count)
            {
                return false;
            }

            for (int i = start; i <= end; i++)
            {
                
                switch (type)
                {
                    case CrashType.REAL:
                        if (rows[i].IsMoving)
                        {
                            return false;
                        }
                        break;
                    case CrashType.FAKE:
                        if (rows[i].IsFakeMoving)
                        {
                            return false;
                        }
                        break;
                    case CrashType.RANDOM:
                        if (rows[i].IsRandomMoving)
                        {
                            return false;
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("type");
                }

            }

            if (type == CrashType.FAKE)
            {
                
                start = index - WINDOW_SIZE_CHECK_CRASH;
                end = index - 1;

                if (start < 0)
                {
                    return false;
                }

                if (!rows[index - 1].IsFakeMoving)
                {
                    return false;
                }

                for (int i = start; i <= end; i++)
                {
                    if (!rows[i].IsMoving)
                    {
                        return false;
                    }
                }

            }

            return true;
        }

        static bool IsPresentCrash(List<Row> rows, CrashType type)
        {
            for (int i = 0; i < rows.Count; i++)
            {
                switch (type)
                {
                    case CrashType.REAL:
                        if (rows[i].IsCrash)
                        {
                            return true;
                        }
                        break;
                    case CrashType.FAKE:
                        if (rows[i].IsFakeCrash)
                        {
                            return true;
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("type");
                }
            }

            return false;
        }

        static bool IsUnCrash(List<Row> rows, CrashType type, int index, int isMovingWindow)
        {

            if (index < 0 || index >= rows.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            var start = index;
            var end = index + isMovingWindow - 1;

            if (end >= rows.Count || start >= rows.Count)
            {
                return false;
            }

            for (int i = start; i <= end; i++)
            {
                switch (type)
                {
                    case CrashType.REAL:
                        if (!rows[i].IsMoving)
                        {
                            return false;
                        }
                        break;
                    case CrashType.FAKE:
                        if (rows[i].Movement <= THRESHOLD_MOVEMENT_FAKE_CRASH)
                        {
                            return false;
                        }
                        break;
                    case CrashType.RANDOM:
                        if (rows[i].Movement <= THRESHOLD_MOVEMENT_RANDOM_SAMPLE)
                        {
                            return false;
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("type");
                }
            }

            return true;

        }

        static int FindNextUnCrash(List<Row> rows, CrashType type, int isMovingWindow)
        {
            for (int i = 0; i < rows.Count; i++)
            {
                if (IsUnCrash(rows, type, i, isMovingWindow))
                {
                    return i;
                }
            }

            return -1;   
        }

        static List<Row> GetTransponderRows(List<Row> rows, string transponderId)
        {
            var transponderRows = new List<Row>();

            for (int i = 0; i < rows.Count; i++)
            {
                if (string.Equals(rows[i].TransponderId, transponderId, StringComparison.OrdinalIgnoreCase))
                {
                    transponderRows.Add(rows[i]);
                }
            }

            return transponderRows;
        }

        static bool IsAnotherHeat(DateTime first, DateTime second, int minutes)
        {
            return (second - first).TotalMinutes > minutes;
        }

        static bool MinimumHeatLength(DateTime first, DateTime second, int minMinutes)
        {
            return (second - first).TotalMinutes >= minMinutes;
        }

        static Pair<int,int> FilterHeat(List<Row> rows, int start, int end)
        {

            for (int i = start; i <= end; i++)
            {
                if (rows[i].DistanceFromTheNearestPit > 0)
                {
                    start = i;
                    break;
                }
            }

            for (int i = end; i >= start; i--)
            {
                if (rows[i].DistanceFromTheNearestPit > 0)
                {
                    end = i;
                    break;
                }
            }

            return new Pair<int, int>(start, end);

        }

        static List<Pair<int,int>> GetHeats(List<Row> rows, int minutes)
        {

            var heats = new List<Pair<int, int>>();

            var start = 0;
            var end = 0;

            for (int i = 1; i < rows.Count; i++)
            {
                if (IsAnotherHeat(rows[i - 1].Timestamp, rows[i].Timestamp, minutes))
                {
                    end = i - 1;

                    if (MinimumHeatLength(rows[start].Timestamp, rows[end].Timestamp, 5))
                    {
                        heats.Add(FilterHeat(rows, start, end));
                    }

                    start = i;
                }
            }

            end = rows.Count - 1;

            if (MinimumHeatLength(rows[start].Timestamp, rows[end].Timestamp, 5))
            {
                heats.Add(FilterHeat(rows, start, end));
            }

            return heats;

        }

        static void MarkCrashes(List<Row> rows, CrashType type, int crashWindow, int unCrashWindow)
        {

            for (int i = 0; i < rows.Count; i++)
            {

                if (i == 0)
                {
                    var nextUnCrash = FindNextUnCrash(rows.GetRange(i, rows.Count - i), type, unCrashWindow);
                
                    if (nextUnCrash != -1)
                    {
                        i = i + nextUnCrash;
                    }
                
                }


                if (IsCrash(rows, type, i, crashWindow))
                {   

                    switch (type)
                    {
                        case CrashType.REAL:
                            rows[i].IsCrash = true;
                            break;
                        case CrashType.FAKE:
                            rows[i].IsFakeCrash = true;
                            break;
                        case CrashType.RANDOM:
                            rows[i].IsRandomCrash = true;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException("type");
                    }
                    
                    var nextUnCrash = FindNextUnCrash(rows.GetRange(i, rows.Count - i), type, unCrashWindow);

                    if (nextUnCrash != -1)
                    {
                        i = i + nextUnCrash;
                    }

                    if (i >= rows.Count || nextUnCrash == -1)
                    {
                        break;
                    }

                }

            }

        }

        static bool IsSlowDown(List<Row> rows, int index, int windowSize)
        {
            
            if (index + windowSize >= rows.Count)
            {
                return false;
            }

            var countFakeMoving = 0;

            for (int i = index; i < index + windowSize; i++)
            {
                if (!rows[i].IsFakeMoving)
                {
                    countFakeMoving++;
                }

                if (!rows[i].IsMoving)
                {
                    return false;
                }

            }

            return countFakeMoving >= (windowSize * 0.7);

        }

        static void MarkSlowDowns(List<Row> rows, int windowSize)
        {

            for (int i = 0; i < rows.Count; i++)
            {

                if (IsSlowDown(rows, i, windowSize))
                {
                    rows[i].IsSlowDown = true;

                    var nextUnCrash = FindNextUnCrash(rows.GetRange(i, rows.Count - i), CrashType.FAKE, windowSize);

                    if (nextUnCrash != -1)
                    {
                        i = i + nextUnCrash;
                    }

                    if (i >= rows.Count || nextUnCrash == -1)
                    {
                        break;
                    }

                }

            }

        }

        static void MarkSteering(List<Row> rows, Pair<int, int> steeringWindowSize, int windowSize)
        {

            var subsequentNotMoving = 0;

            for (int i = 0; i < rows.Count; i++)
            {
                
                if (rows[i].IsMoving)
                {
                    subsequentNotMoving = 0;
                    continue;
                }

                subsequentNotMoving++;

                if (subsequentNotMoving <= steeringWindowSize.Second && subsequentNotMoving >= steeringWindowSize.First)
                {

                    var _steeringWindowSize = subsequentNotMoving;
                    var subsequentMoving = true;

                    for (int j = (i - windowSize - _steeringWindowSize) + 1; j <= i - _steeringWindowSize; j++)
                    {
                        if (j < 0 || j >= rows.Count)
                        {
                            subsequentMoving = false;
                            break;
                        }

                        if (!rows[j].IsMoving)
                        {
                            subsequentMoving = false;
                            break;
                        }

                    }

                    for (int j = i + 1; j <= i + windowSize; j++)
                    {
                        if (j < 0 || j >= rows.Count)
                        {
                            subsequentMoving = false;
                            break;
                        }

                        if (!rows[j].IsMoving)
                        {
                            subsequentMoving = false;
                            break;
                        }

                    }

                    if (!subsequentMoving)
                    {
                        continue;
                    }

                    var start = (i - windowSize - _steeringWindowSize) + 2;
                    var end = (i - windowSize) + 1;

                    for (int j = start; j <= end; j++)
                    {
                        rows[(j + windowSize) - 1].Steering = true;
                    }
                }

            }

        }

        static List<Pair<int,int>> GetWindowsIndices(List<Row> rows, CrashType type, int windowSize)
        {

            var windows = new List<Pair<int, int>>();
            var alpha = EVENT_SHIFT;

            for (int i = 0; i < rows.Count; i++)
            {
                if (((type == CrashType.REAL && rows[i].IsCrash) || (type == CrashType.FAKE && rows[i].IsFakeCrash) || (type == CrashType.RANDOM && rows[i].IsRandomCrash)) &&
                    rows[i].DistanceFromTheNearestPit > THRESHOLD_NEAREST_PIT)
                {

                    for (int j = (i - alpha); j <= (i + alpha); j++)
                    {
                        if (j < 0 || j >= rows.Count)
                        {
                            continue;
                        }

                        windows.Add(new Pair<int, int>(j - windowSize + 1, j));
                    }

                }
            }

            return windows;

        }

        static List<Pair<int,int>> GetSteeringWindowsIndices(List<Row> rows, int windowSize)
        {

            var windows = new List<Pair<int, int>>();

            for (int i = 0; i < rows.Count; i++)
            {
                if (rows[i].Steering && rows[i].DistanceFromTheNearestPit > THRESHOLD_NEAREST_PIT)
                {
                    windows.Add(new Pair<int, int>(i - windowSize + 1, i));
                }
            }

            return windows;

        }

        static List<Pair<int,int>> GetSlowDownsWindowsIndices(List<Row> rows, int outputWindowSize)
        {

            var windows = new List<Pair<int, int>>();

            for (int i = 0; i < rows.Count; i++)
            {
                if (rows[i].IsSlowDown)
                {
                    windows.Add(new Pair<int, int>(i, i + outputWindowSize - 1));
                }
            }

            return windows;

        }



        static List<List<Row>> GetWindows(List<Row> rows, int outputWindowSize)
        {

            var crashWindows = GetWindowsIndices(rows, CrashType.REAL, outputWindowSize);
            var fakeCrashWindows = GetWindowsIndices(rows, CrashType.FAKE, outputWindowSize);
            var randomCrashWindows = GetWindowsIndices(rows, CrashType.RANDOM, outputWindowSize);
            var slowDownWindows = GetSlowDownsWindowsIndices(rows, outputWindowSize);
            var steeringWindows = GetSteeringWindowsIndices(rows, outputWindowSize);

            var windows = new List<List<Row>>();

            foreach (Pair<int, int> window in crashWindows)
            {
                var start = window.First;
                var end = window.Second;

                if (start < 0)
                {
                    continue;
                }

                var windowList = new List<Row>();

                for (int i = start; i <= end; i++)
                {
                    var row = (Row) rows[i].Clone();
                    row.WindowLabel = true;
                    row.Window = _windowCount;
                    row.Type = "Crash";

                    windowList.Add(row);
                }

                windows.Add(windowList);
                _windowCount++;

            }

            foreach (Pair<int, int> window in fakeCrashWindows)
            {
                var start = window.First;
                var end = window.Second;

                if (start < 0)
                {
                    continue;
                }

                var windowList = new List<Row>();

                for (int i = start; i <= end; i++)
                {
                    var row = (Row) rows[i].Clone();
                    row.WindowLabel = false;
                    row.Window = _windowCount;
                    row.Type = "Negative";

                    windowList.Add(row);
                }

                windows.Add(windowList);
                _windowCount++;

            }

            foreach (Pair<int, int> window in randomCrashWindows)
            {
                var start = window.First;
                var end = window.Second;

                if (start < 0)
                {
                    continue;
                }

                var windowList = new List<Row>();

                for (int i = start; i <= end; i++)
                {
                    var row = (Row) rows[i].Clone();
                    row.WindowLabel = false;
                    row.Window = _windowCount;
                    row.Type = "Random";

                    windowList.Add(row);
                }

                windows.Add(windowList);
                _windowCount++;

            }

            foreach (Pair<int, int> window in steeringWindows)
            {
                var start = window.First;
                var end = window.Second;

                if (start < 0)
                {
                    continue;
                }

                var windowList = new List<Row>();

                for (int i = start; i <= end; i++)
                {
                    var row = (Row) rows[i].Clone();
                    row.WindowLabel = false;
                    row.Window = _windowCount;
                    row.Type = "Drifting";

                    if (i == end)
                        row.Steering = true;

                    windowList.Add(row);
                }

                windows.Add(windowList);
                _windowCount++;
            }

            foreach (Pair<int, int> window in slowDownWindows)
            {
                var start = window.First;
                var end = window.Second;

                if (start < 0)
                {
                    continue;
                }

                var windowList = new List<Row>();

                for (int i = start; i <= end; i++)
                {
                    var row = (Row) rows[i].Clone();
                    row.WindowLabel = false;
                    row.Window = _windowCount;
                    row.Type = "SlowDown";

                    windowList.Add(row);
                }

                windows.Add(windowList);
                _windowCount++;
            }

            return windows;
        }

        static Pair<int, List<Pair<int, int>>> GetWindowsByPosition(List<Pair<int, int>> windows, List<Row> rows, CrashType type, int position)
        {

            var alpha = (EVENT_SHIFT * 2) * 2;
            var windowList = new List<Pair<int, int>>();

            for (int i = position; i < windows.Count; i += alpha + 1)
            {
                var start = windows[i].First;
                var end = windows[i].Second;

                windowList.Add(new Pair<int, int>(start, end));

            }

            return new Pair<int, List<Pair<int, int>>>(position, windowList);

        }

        static List<Pair<int, List<Row>>> GetTestWindows(List<Row> rows, int outputWindowSize)
        {

            var totalWindowShifting = EVENT_SHIFT * 2 + 1;

            var crashWindows = GetWindowsIndices(rows, CrashType.REAL, outputWindowSize);
            var fakeCrashWindows = GetWindowsIndices(rows, CrashType.FAKE, outputWindowSize);
            var randomCrashWindows = GetWindowsIndices(rows, CrashType.RANDOM, outputWindowSize);
            var steeringWindows = GetSteeringWindowsIndices(rows, outputWindowSize);
            var slowDownWindows = GetSlowDownsWindowsIndices(rows, outputWindowSize);

            var testWindows = new List<Pair<int, List<Row>>>();

            for (int i = 0; i < totalWindowShifting; i++)
            {
                var windows = GetWindowsByPosition(crashWindows, rows, CrashType.REAL, i);

                var windowList = new List<Row>();

                foreach (Pair<int, int> window in windows.Second)
                {
                    var start = window.First;
                    var end = window.Second;

                    if (start < 0)
                    {
                        continue;
                    }

                    for (int j = start; j <= end; j++)
                    {
                        var row = (Row) rows[j].Clone();
                        row.WindowLabel = true;
                        row.Window = _windowCount;
                        row.Type = "Crash";

                        windowList.Add(row);
                    }

                    _windowCount++;

                }

                testWindows.Add(new Pair<int, List<Row>>(windows.First, windowList));

            }

            for (int i = 0; i < totalWindowShifting; i++)
            {

                var windows = GetWindowsByPosition(fakeCrashWindows, rows, CrashType.FAKE, i);

                var windowList = new List<Row>();

                foreach (Pair<int, int> window in windows.Second)
                {
                    var start = window.First;
                    var end = window.Second;

                    if (start < 0)
                    {
                        continue;
                    }

                    for (int j = start; j <= end; j++)
                    {
                        var row = (Row) rows[j].Clone();
                        row.WindowLabel = false;
                        row.Window = _windowCount;
                        row.Type = "Negative";

                        windowList.Add(row);
                    }

                    _windowCount++;

                }

                testWindows.Add(new Pair<int, List<Row>>(windows.First + totalWindowShifting, windowList));

            }

            var negativeWindows = new List<Row>();

            foreach (Pair<int, int> window in steeringWindows)
            {
                var start = window.First;
                var end = window.Second;

                if (start < 0)
                {
                    continue;
                }

                for (int i = start; i <= end; i++)
                {
                    var row = (Row) rows[i].Clone();
                    row.WindowLabel = false;
                    row.Window = _windowCount;
                    row.Type = "Drifting";

                    negativeWindows.Add(row);
                }

                _windowCount++;

            }


            testWindows.Add(new Pair<int, List<Row>>((totalWindowShifting * 2), negativeWindows));

            negativeWindows = new List<Row>();

            foreach (Pair<int, int> window in randomCrashWindows)
            {
                var start = window.First;
                var end = window.Second;

                if (start < 0)
                {
                    continue;
                }

                for (int i = start; i <= end; i++)
                {
                    var row = (Row) rows[i].Clone();
                    row.WindowLabel = false;
                    row.Window = _windowCount;
                    row.Type = "Random";

                    negativeWindows.Add(row);
                }

                _windowCount++;

            }

            testWindows.Add(new Pair<int, List<Row>>((totalWindowShifting * 2) + 1, negativeWindows));

            negativeWindows = new List<Row>();

            foreach (Pair<int, int> window in slowDownWindows)
            {
                var start = window.First;
                var end = window.Second;

                for (int i = start; i <= end; i++)
                {
                    var row = (Row) rows[i].Clone();
                    row.WindowLabel = false;
                    row.Window = _windowCount;
                    row.Type = "SlowDown";

                    negativeWindows.Add(row);
                }

                _windowCount++;

            }

            testWindows.Add(new Pair<int, List<Row>>((totalWindowShifting * 2) + 2, negativeWindows));

            return testWindows;

        }

        static void MergeTestSets(string inputPath, string outputPath)
        {

            var files = Directory.GetFiles(inputPath);

            var lines = new List<string>();

            foreach (string file in files)
            {
                
                if (!file.Contains("RTLS-test"))
                    continue;

                var fileLines = File.ReadAllLines(file);

                for (int i = 1; i < fileLines.Length; i++)
                {
                    lines.Add(fileLines[i]);
                }

            }

            using (StreamWriter sw = File.CreateText(outputPath + "\\RTLS-test.csv"))
            {
                sw.WriteLine(string.Join(",", Columns.GetDatasetColumns()));

                foreach (string line in lines)
                {
                    sw.WriteLine(line);
                }

            }

        }

        static void SplitTrainingSet(string inputPath, string outputPath, int splitCount)
        {

            var lines = File.ReadAllLines(inputPath);
            var numOfWindows = lines[lines.Length - 1].Split(',')[15];

            var splitSize = int.Parse(numOfWindows) / splitCount;

            for (int i = 0; i < splitCount; i++)
            {
                var splitLines = new List<string>();

                for (int j = 0; j < lines.Length; j++)
                {
                    var line = lines[j];
                    var window = line.Split(',')[15];

                    if (window == "Window")
                        continue;

                    if (int.Parse(window) >= i * splitSize && int.Parse(window) < (i + 1) * splitSize)
                    {
                        splitLines.Add(line);
                    }

                }

                using (StreamWriter sw = File.CreateText(outputPath + "\\RTLS-train-" + i + ".csv"))
                {
                    sw.WriteLine(string.Join(",", Columns.GetDatasetColumns()));

                    foreach (string line in splitLines)
                    {
                        sw.WriteLine(line);
                    }
                }

            }

        }

        static void Main(string[] args)
        {


            var config = new Config();

            var logsFiles = Directory.GetFiles(config.LogsPath);
            var testSetFiles = Directory.GetFiles(config.TestSetPath);

            var files = testSetFiles.Concat(logsFiles).ToArray();
            
            Pair<int, int> BLANK_SIZE_STEERING = new Pair<int, int>(2, 3);


            if (!Directory.Exists(config.DatasetOutputPath))
            {
                Directory.CreateDirectory(config.DatasetOutputPath);
                System.Console.WriteLine("[INFO] Created directory: " + config.DatasetOutputPath);
            }


            if (config.Mode == ModeType.ALL || config.Mode == ModeType.DATASET)
            {
                using (StreamWriter sw = File.CreateText(config.DatasetOutputPath + "\\RTLS-train.csv"))
                {
                    sw.WriteLine(string.Join(",", Columns.GetDatasetColumns()));
                }
            }


            if (config.Mode == ModeType.ALL || config.Mode == ModeType.DATASET)
            {
                for (int i = 0; i <= ((EVENT_SHIFT * 2) * 2) + 4; i++)
                {
                    using (StreamWriter sw = File.CreateText(config.DatasetOutputPath + "\\RTLS-test-" + i + ".csv"))
                    {
                        sw.WriteLine(string.Join(",", Columns.GetDatasetColumns()));

                    }
                }
            }


            foreach (string file in files)
            {

                var rawData = new Dictionary<string, RawData>();
                var processedData = new Dictionary<string, ProcessedData>();

                if (file.EndsWith(".txt"))
                {

                    using (StreamReader sr = File.OpenText(file))
                    {
                        var s = "";

                        while ((s = sr.ReadLine()) != null)
                        {

                            if (s.StartsWith("TP"))
                            {

                                var line = s.Split(',');

                                if (line.Length >= 11)
                                {
                                    Point3D point = ParsePoint(line[10]);

                                    line[7] = line[7].Replace('T', ' ');
                                    line[7] = line[7].Replace('-', '/');
                                    line[7] = line[7].Replace('.', ':');
                                    
                                    if (line[7].Length != 23)
                                        continue;

                                    if (line[7].Length > 23)
                                        line[7] = line[7].Substring(0, 23);

                                    var key = line[1] + '-' + line[7];

                                    if (!rawData.ContainsKey(key))
                                    {

                                        if (double.TryParse(line[3], out double t1) &&
                                            double.TryParse(line[4], out double t2))
                                        {

                                            rawData.Add(
                                                key,
                                                new RawData(line[1], line[3], line[4], line[7], point)
                                            );

                                        }
                                    }

                                }

                            }
                            else if (s.StartsWith("|"))
                            {

                                var partitions = s.Split('\t');
                                partitions[0] = partitions[0].Replace("|", "");

                                var line = partitions[0].Split(',');

                                if (line.Length >= 12)
                                {
                                    var key = line[0] + '-' + line[1];
                                    key = key.Substring(0, key.Length - 1);

                                    var isCrashEvent = false;

                                    for (int i = 1; i < partitions.Length; i++)
                                    {
                                        var partition = partitions[i].Split('|');

                                        if (partition.Length == 2)
                                        {
                                            if (partition[0] == "1")
                                            {
                                                isCrashEvent = true;
                                            }
                                        }
                                    }

                                    if (!processedData.ContainsKey(key))
                                    {

                                        if (double.TryParse(line[2], out double t1) &&
                                            double.TryParse(line[3], out double t2) &&
                                            double.TryParse(line[4], out double t3) &&
                                            double.TryParse(line[8], out double t4) &&
                                            double.TryParse(line[9], out double t5) &&
                                            double.TryParse(line[10], out double t6) &&
                                            double.TryParse(line[11], out double t7))
                                        {
                                            if (line[2].Count(x => x == '.') > 1 ||
                                                line[3].Count(x => x == '.') > 1 ||
                                                line[4].Count(x => x == '.') > 1 ||
                                                line[8].Count(x => x == '.') > 1 ||
                                                line[9].Count(x => x == '.') > 1 ||
                                                line[10].Count(x => x == '.') > 1 ||
                                                line[11].Count(x => x == '.') > 1)
                                            {
                                                continue;
                                            }

                                            processedData.Add(
                                                key,
                                                new ProcessedData(line[0], line[2], line[3], line[4], line[8], line[9], line[10], line[11], isCrashEvent ? "1" : "0")
                                            );

                                        }

                                    }

                                }

                            }
                        }
                    }
                }

                System.Console.WriteLine("[INFO] Parsing file: " + file + " done.");

                var rows = new List<Row>();

                foreach (var key in rawData.Keys)
                {

                    if (processedData.ContainsKey(key))
                    {

                        var raw = rawData[key];
                        var processed = processedData[key];

                        rows.Add(new Row(
                            raw.TransponderId,
                            raw.RawPositionX,
                            raw.RawPositionY,
                            raw.Timestamp,
                            raw.Accelerometer,
                            processed.X,
                            processed.Y,
                            processed.IsMoving,
                            processed.Progress,
                            processed.DistanceFromTheNearestPit,
                            processed.Speed,
                            processed.SignalExpired,
                            processed.Movement,
                            processed.IsCrashEvent,
                            processed.IsFakeMoving,
                            processed.IsRandomMoving
                        ));
                        
                    }

                }

                var transponders = GetAllTransponder(rows);

                foreach (var transponder in transponders)
                {

                    var transponderRows = GetTransponderRows(rows, transponder);
                    var heats = GetHeats(transponderRows, 1);
                    var heatsCount = 0;

                    foreach (var heat in heats)
                    {

                        var heatRows = transponderRows.GetRange(heat.First, heat.Second - heat.First + 1);

                        MarkCrashes(heatRows, CrashType.REAL, WINDOW_SIZE_CRASH, WINDOW_SIZE_UNCRASH);
                        MarkCrashes(heatRows, CrashType.FAKE, WINDOW_SIZE_CRASH, WINDOW_SIZE_UNCRASH);
                        MarkSteering(heatRows, BLANK_SIZE_STEERING, WINDOW_SIZE_CHECK_CRASH);

                        MarkCrashes(heatRows, CrashType.RANDOM, WINDOW_SIZE_CRASH, WINDOW_SIZE_UNCRASH);
                        MarkSlowDowns(heatRows, WINDOW_SIZE_OUTPUT);

                        if (IsPresentCrash(heatRows, CrashType.REAL) && IsPresentCrash(heatRows, CrashType.FAKE))
                        {

                            if (config.Mode == ModeType.ALL || config.Mode == ModeType.HEATS)
                            {
                                var path = file.StartsWith(config.LogsPath) ? config.LogsPath : config.TestSetPath;
                                var fileName = file.Replace(path + "\\", "").Replace(".txt", "") + "_" + transponder.Replace(":", "-") + "_" + heatsCount + ".csv";

                                using (StreamWriter sw = File.CreateText(config.HeatsOutputPath + "\\" + fileName))
                                {
                                    sw.WriteLine(string.Join(",", Columns.GetHeatsColumns()));

                                    for (int i = 0; i < heatRows.Count; i++)
                                    {
                                        sw.WriteLine(string.Join(",", heatRows[i].GetHeatsValues()));
                                    }

                                }

                                heatsCount++;
                            }


                            if ((config.Mode == ModeType.ALL || config.Mode == ModeType.DATASET) && file.StartsWith(config.LogsPath))
                            {

                                var windows = GetWindows(heatRows, WINDOW_SIZE_OUTPUT);

                                foreach (var window in windows)
                                {
                                    foreach (var row in window)
                                    {
                                        using (StreamWriter sw = File.AppendText(config.DatasetOutputPath + "\\RTLS-train.csv"))
                                        {
                                            sw.WriteLine(string.Join(",", row.GetDatasetValues()));
                                        }
                                    }

                                }

                            }


                            if ((config.Mode == ModeType.ALL || config.Mode == ModeType.DATASET) && file.StartsWith(config.TestSetPath))
                            {

                                var windows = GetTestWindows(heatRows, WINDOW_SIZE_OUTPUT);

                                foreach (var window in windows)
                                {
                                    
                                    foreach (var row in window.Second)
                                    {
                                        using (StreamWriter sw = File.AppendText(config.DatasetOutputPath + "\\RTLS-test-" + window.First + ".csv"))
                                        {
                                            sw.WriteLine(string.Join(",", row.GetDatasetValues()));
                                        }
                                    }
                                    
                                }
                                
                            }

                        }

                    }

                }

                System.Console.WriteLine("[INFO] " + file + " processed.");

            }


            if (config.Mode == ModeType.ALL || config.Mode == ModeType.DATASET)
            {
                System.Console.WriteLine("[INFO] Training Set created.");
            }

            if (config.Mode == ModeType.ALL || config.Mode == ModeType.DATASET)
            {
                MergeTestSets(config.DatasetOutputPath, config.DatasetOutputPath);
                System.Console.WriteLine("[INFO] Test Set created.");
            }

            if (config.Mode == ModeType.ALL || config.Mode == ModeType.HEATS)
            {
                System.Console.WriteLine("[INFO] Heats file created.");
            }

            if ((config.Mode == ModeType.ALL || config.Mode == ModeType.DATASET) && config.SplitTrainingSet)
            {
                SplitTrainingSet(config.DatasetOutputPath + "\\RTLS-train.csv", config.DatasetOutputPath, SPLIT_FACTOR);
                System.Console.WriteLine("[INFO] Training Set splitted.");
            }

            System.Console.WriteLine("[INFO] Done.");

        }

    }

}

