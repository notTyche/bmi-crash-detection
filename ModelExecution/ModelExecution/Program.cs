using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Transforms.Onnx;

class Program {

    static string ONNX_MODEL_PATH = "C:\\Users\\perfi\\OneDrive\\Desktop\\model.onnx"; // Change to your model path

    static float ACCELEROMETER_X_MEAN = 1272.052966516978f;
    static float ACCELEROMETER_X_STD = 50050828.36716162f;
    static float ACCELEROMETER_Y_MEAN = -6306.2853670527065f;
    static float ACCELEROMETER_Y_STD = 128757779.81743118f;
    static float ACCELEROMETER_Z_MEAN = -9017.849340581422f;
    static float ACCELEROMETER_Z_STD = 22078912.024569385f;
    static float MOVEMENT_MEAN = 8091.834744189711f;
    static float MOVEMENT_STD = 74370201.01164806f;


    public class Window {

        [ColumnName("Accelerometer_x")]
        public float[]? Accelerometer_x { get; set; }

        [ColumnName("Accelerometer_y")]
        public float[]? Accelerometer_y { get; set; }

        [ColumnName("Accelerometer_z")]
        public float[]? Accelerometer_z { get; set; }

        [ColumnName("Movement")]
        public float[]? Movement { get; set; }

        public void Standardize() {

            for (int i = 0; i < Accelerometer_x.Length; i++) {
                Accelerometer_x[i] = (Accelerometer_x[i] - ACCELEROMETER_X_MEAN) / ACCELEROMETER_X_STD;
                Accelerometer_y[i] = (Accelerometer_y[i] - ACCELEROMETER_Y_MEAN) / ACCELEROMETER_Y_STD;
                Accelerometer_z[i] = (Accelerometer_z[i] - ACCELEROMETER_Z_MEAN) / ACCELEROMETER_Z_STD;
                Movement[i] = (Movement[i] - MOVEMENT_MEAN) / MOVEMENT_STD;
            }

        }

        public float[] Flatten() {

            var flattened = new float[Accelerometer_x.Length * 4];
            var index = 0;

            for (int i = 0; i < Accelerometer_x.Length; i++) {
                flattened[index++] = Accelerometer_x[i];
                flattened[index++] = Accelerometer_y[i];
                flattened[index++] = Accelerometer_z[i];
                flattened[index++] = Movement[i];
            }

            return flattened;
            
        }

    }

    static ITransformer GetPredictionPipeline(MLContext mlContext)
    {
        var pipeline = mlContext.Transforms.ApplyOnnxModel(
                   modelFile: ONNX_MODEL_PATH,
                              inputColumnNames: new[] { "input_1" },
                              outputColumnNames: new[] { "dense_10" });

        return pipeline.Fit(mlContext.Data.LoadFromEnumerable(new List<OnnxInput>()));
    }

    static void Main(string[] args)
    {

        MLContext mlContext = new MLContext();

        var model = GetPredictionPipeline(mlContext);

        var predictionEngine = mlContext.Model.CreatePredictionEngine<OnnxInput, OnnxOutput>(model);

        var window = new Window() {
            Accelerometer_x = new float[] { 14573, 1501, -859, 12425, 4053, -367, -3923, -13503, 8377, 8597, 13965, 5321, 7169, 793, -263, -1199, -239, -227, -1567, -551, -1023, -1187, -1311, -1335, -1227, -891, -2547, 17, -1531, -1495, 1417, 933, 425, 1453, -2339, -1531, -2287, 289, -703, -1223, -1703, 481, -2331, -1503, -3527, -2303, -4251, -1979, -3027, -3463 },
            Accelerometer_y = new float[] { -25827, -22067, -24691, -12235, -22119, -23195, -30391, -20247, -12303, -18167, -7455, -20959, -8011, -10955, -16139, -19251, -17715, -16059, -13415, -13903, -13131, -11967, -10811, -11455, -10143, -8543, -8875, -8315, -6471, -10255, -10247, -11747, -9555, -14039, -14647, -9991, -11083, -12931, -13299, -14275, -10683, -10455, -11039, -12895, -13423, -12027, -14571, -11211, -12575, -12531 },
            Accelerometer_z = new float[] { 4060, -3672, -19768, -32220, -14460, -23208, -19968, -31388, 9752, -7788, -25444, -17992, -10368, -10064, -8292, -9408, -4400, -7052, -9696, -9016, -10364, -12932, -12860, -11940, -12668, -18040, -14664, -14264, -14328, -8544, -9172, -10272, -9680, -13248, -10432, -12000, -9872, -10004, -10872, -10316, -14752, -15692, -13708, -11788, -13828, -9164, -10344, -8516, -12464, -9444 },
            Movement = new float[] { 68368, 43804, 38192, 38192, 36016, 14244, 13992, 31144, 70964, 33736, 33736, 29600, 22420, 9624, 6388, 11552, 7504, 4320, 972, 2592, 2592, 3896, 1352, 1588, 2148, 7308, 3524, 3524, 3456, 9604, 3084, 3084, 9080, 9080, 7216, 7032, 3976, 4556, 2228, 2052, 8508, 3352, 4604, 4604, 4592, 7284, 5672, 6360, 6360, 3500 }
        };

        window.Standardize();

        var input = new OnnxInput() { Features = window.Flatten() };

        var prediction = predictionEngine.Predict(input);

        Console.WriteLine($"Predicted value: {prediction.IsCrash.First()}");

    }

    public class OnnxInput {
        [ColumnName("input_1")]
        [VectorType(50 * 4)]
        public float[]? Features { get; set; }
    }

    public class OnnxOutput {
        [ColumnName("dense_10")]
        public float[]? IsCrash { get; set; }
    }


}